import React from 'react';
import './App.scss';
import Gallery from '../Gallery';

class App extends React.Component {
  static propTypes = {};

  constructor() {
    super();
    this.timeOut = 0;
    this.state = {
      tag: 'art'
    };
  }

  handleSearch = event => {
    let searchText = event.target.value;
    if (this.timeout) clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.setState({
        tag: searchText
      });
    }, 500);
  };

  render() {
    return (
      <div className="app-root">
        <div className="app-header">
          <h2>Flickr Gallery</h2>
          <input
            className="app-input"
            onChange={this.handleSearch}
            value={this.state.tag}
          />
        </div>
        <Gallery tag={this.state.tag} />
      </div>
    );
  }
}

export default App;
