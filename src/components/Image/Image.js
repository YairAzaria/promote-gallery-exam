import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number
  };

  constructor(props){
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      filters: [
        'blur(5px)',
        'brightness(200%)',
        'contrast(200%)',
        'hue-rotate(90deg)',
        'invert(100%)'
      ],
      size: 200
    };
  }

  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = galleryWidth / imagesPerRow;
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${
      dto.secret
    }.jpg`;
  }

  imageClone =()=> {
    this.props.imageClone(this.props.dto,this.props.index);
  }

  filterChange =()=> {
    let getRandomFilter = Math.floor(Math.random() * 5);
    this.setState({ filter: this.state.filters[getRandomFilter] });
  }

  modalControll =()=> {
    this.props.modalControll(this.urlFromDto(this.props.dto));
  };

  render() {
    return (
      <div
        className="image-root"
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px',
          filter: this.state.filter
        }}
      >
        <div>
          <FontAwesome
            className="image-icon"
            name="clone"
            title="clone"
            onClick={this.imageClone}
          />
          <FontAwesome
            className="image-icon"
            name="filter"
            title="filter"
            onClick={this.filterChange}
          />
          <FontAwesome
            className="image-icon"
            name="expand"
            title="expand"
            onClick={this.modalControll}
          />
        </div>
      </div>
    );
  }
}

export default Image;
