import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import FontAwesome from 'react-fontawesome';


class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      imageUrl: '',
      modalOpen: false,
      modalClass: 'myModal modalHide',
      images: [],
      fetchPage:1,
      galleryWidth: this.getGalleryWidth()
    };
  }


  getImages(tag,page) {
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=50&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          if (page===1){
            this.setState({ images: res.photos.photo,fetchPage:2});
          }
          else{
            let nextPage = [...this.state.images,...res.photos.photo];
            this.setState({ images: nextPage,fetchPage:this.state.fetchPage + 1 });
          }
        }
      });
  }

  imageClone =(image, index)=> {
    const imagesCopy = this.state.images;
    imagesCopy.splice(index,0,image);
     this.setState({ images: imagesCopy });
  };

  infiniteScroll =()=> {
    if ((Math.ceil(document.documentElement.scrollTop) + document.documentElement.clientHeight) ===
    document.documentElement.scrollHeight){
    this.getImages(this.props.tag , this.state.fetchPage);
    }
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  componentDidMount() {
    window.addEventListener('scroll', ()=>this.infiniteScroll());
    this.getImages(this.props.tag);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.infiniteScroll);
  }


  componentWillReceiveProps(props) {
    this.getImages(props.tag,1);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
  }

  modalControll =image=> {
    if (!this.state.modalOpen) {
      this.setState({
        modalOpen: true,
        modalClass: 'myModal modalShow',
        imageUrl: image
      });
    } else {
      this.setState({
        modalOpen: false,
        modalClass: 'myModal modalHide'
      });
    }
  };

  render() {
    return (
      <div className='gallery-root flexBox'>
        {this.state.images.map((dto, index) => {
          return (
            <Image
              key={'image-' + index}
              dto={dto}
              imageClone={image => this.imageClone(image,index)}
              index={index}
              modalControll={this.modalControll}
              galleryWidth={this.state.galleryWidth}
            />
          );
        })}
        <div className={this.state.modalClass}>
          <img src={this.state.imageUrl} />
          <div className='closeModal'>
            <FontAwesome
              className='image-icon'
              name='window-close'
              title='close the window'
              onClick={this.modalControll}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Gallery;
